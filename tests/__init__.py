import unittest
import app
from flask import current_app


class BaseTestCase(unittest.TestCase):
    """
    A base class for tests.
    We need the Flask app context for all tests.
    This base class ensures we don't have to open it for every test separately.
    source: http://stackoverflow.com/questions/18564743/can-i-have-all-of-my-unit-tests-run-in-the-flask-app-context
    """
    def __call__(self, result=None):
        self._pre_setup()
        super(BaseTestCase, self).__call__(result)
        self._post_teardown()

    def _pre_setup(self):
        self.app = app.create_apiserver('testing')
        self.app.testing = True
        self.client = self.app.test_client()
        self._ctx = self.app.test_request_context()
        self._ctx.push()

    def _post_teardown(self):
        if getattr(self, '_ctx') and self._ctx is not None:
            self._ctx.pop()
        del self._ctx
