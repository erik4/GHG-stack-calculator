from tests import BaseTestCase, unittest
from app.utils.stackmanager import ListStackManager, ListStack


class TestCalculationLogic(BaseTestCase):

    def setUp(self):
        pass

    def test_stack_base_class_push_pop(self):
        """
           Tests the push and pop functionality of the stack base class
        """
        bsm = ListStackManager()
        bsm.stacks.append(ListStack())
        bsm[0].push(2)
        bsm[0].push(-1)
        bsm[0].push(3)
        last_item = bsm[0].pop()

        self.assertEqual(last_item, 3)
        self.assertEqual(bsm.stacks[0]._stack, [2, -1])

    def test_peek(self):
        """
           Tests the peek function
        """
        from app.businesslogic.calc import CalculationLogic

        bsm = ListStackManager()
        stack0 = ListStack(0, 1, 2, 3, 4, 5, 6)
        stack1 = ListStack(-1, -2, -3, -4, -5, -6)
        bsm.stacks.append(stack0)
        bsm.stacks.append(stack1)

        top_of_stack0 = CalculationLogic.peek(bsm[0])
        top_of_stack1 = CalculationLogic.peek(bsm[1])

        self.assertEqual(top_of_stack0, 6)
        self.assertEqual(top_of_stack1, -6)

    def test_add(self):
        """
           Tests the add function
        """
        from app.businesslogic.calc import CalculationLogic

        bsm = ListStackManager()
        stack0 = ListStack(0, 1, 2, 3, 4, 5, 6)
        bsm.stacks.append(stack0)

        the_sum = CalculationLogic.add(bsm[0])

        self.assertEqual(the_sum, 5+6)

    def test_subtract(self):
        """
           Tests the subtract function
        """
        from app.businesslogic.calc import CalculationLogic

        bsm = ListStackManager()
        stack0 = ListStack(0, 1, 2, 3, 4, 5, 6)
        bsm.stacks.append(stack0)

        the_sum = CalculationLogic.subtract(bsm[0])

        self.assertEqual(the_sum, 5-6)

    def test_multiply(self):
        """
           Tests the multiply function
        """
        from app.businesslogic.calc import CalculationLogic

        bsm = ListStackManager()
        stack0 = ListStack(0, 1, 2, 3, 4, 5, 6)
        bsm.stacks.append(stack0)

        the_division = CalculationLogic.multiply(bsm[0])

        self.assertEqual(the_division, 5*6)

    def test_divide(self):
        """
           Tests the divide function
        """
        from app.businesslogic.calc import CalculationLogic

        bsm = ListStackManager()
        stack0 = ListStack(0, 1, 2, 3, 4, 5, 6)
        bsm.stacks.append(stack0)

        the_division = CalculationLogic.divide(bsm[0])

        self.assertEqual(the_division, 5//6)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
