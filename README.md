# GHG-stack-calculator

A stack calculator via HTTP endpoints.

## Prerequisites:
 * [Python 3.x][1]
 * [Redis][2]

## Installation:
This is a Flask/Python app. It's recommended you install it in a [virtual environment][3].
To install the application:

 * Download the code to a location on your system
 * Create a virtual environment (recommended) and activate it
 * Install the required libraries using pip, either globally, or in your virtual environment.
 To do so, use the following command in the installation folder:`pip install -r requirements.txt`
 * specify your Redis server in a *config.py* file in the `instance` folder like this:

```
    REDIS_HOST = "localhost"
    REDIS_DB = 1
```

## usage:
To start the server use the following command in the installation folder:
`python apiserver.py`

The server will run on your localhost port 5000.
There are several commands available using HTTP API calls.

 * `/calc/:id/peek` **HTTP GET** - returns stack[top]
 * `/calc/:id/push/<n>` **HTTP POST** - pushes a number onto the stack
 * `/calc/:id/pop` **HTTP DELETE** - returns the top from the stack and removes it
 * `/calc/:id/add` **HTTP PATCH** - removes the top and top-1 from the stack and replaces it with stack[top-1]+stack[top]
 * `/calc/:id/subtract` **HTTP PATCH** - removes the top and top-1 from the stack and replaces it with stack[top-1]-stack[top]
 * `/calc/:id/multiply` **HTTP PATCH** - removes the top and top-1 from the stack and replaces it with stack[top-1]*stack[top]
 * `/calc/:id/divide` **HTTP PATCH** - removes the top and top-1 from the stack and replaces it with stack[top-1]/stack[top]

Where ':id' is an integer representing a specific stack.

You will find a file called `GHG calc.postman_collection.json` in the root folder of the application. This file can be imported in the [Postman][4] application to test/use the provided API calls.


  [1]: https://www.python.org/downloads/
  [2]: https://redis.io/
  [3]: https://realpython.com/python-virtual-environments-a-primer/
  [4]: https://www.getpostman.com/apps
  