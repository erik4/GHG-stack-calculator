from flask_restful import Resource
from app.utils.stackmanager import RedisStackManager
from app.businesslogic.calc import CalculationLogic

rsm = RedisStackManager()


class Peek(Resource):
    def get(self, stackidx):
        return CalculationLogic.peek(rsm[stackidx])


class Push(Resource):
    def post(self, stackidx, number):
        # to accomodate negatives, the number parameter in the URL is defined as a string
        # we need to turn it back into a number
        nr = int(number)
        return CalculationLogic.push(rsm[stackidx], nr)


class Pop(Resource):
    def delete(self, stackidx):
        return CalculationLogic.pop(rsm[stackidx])


class Add(Resource):
    def patch(self, stackidx):
        return CalculationLogic.add(rsm[stackidx])


class Subtract(Resource):
    def patch(self, stackidx):
        return CalculationLogic.subtract(rsm[stackidx])


class Multiply(Resource):
    def patch(self, stackidx):
        return CalculationLogic.multiply(rsm[stackidx])


class Divide(Resource):
    def patch(self, stackidx):
        return CalculationLogic.divide(rsm[stackidx])
