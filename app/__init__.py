import os
from flask import Flask
from flask_restful import Api
from app.initialise import setup_logging
from app.configuration import load_configuration
from pathlib import Path


def create_apiserver(config_name, application_name=__name__, settings_override=None):
    """
    Create an application using the Flask app factory pattern:
    http://flask.pocoo.org/docs/0.10/patterns/appfactories

    :param application_name: Name of the application
    :param settings_override: Override settings
    :type settings_override: dict
    :return: Flask app
    """
    from app.api import add_resources

    app_dir = os.path.dirname(os.path.abspath(__file__))
    base_dir = str(Path(app_dir).parent)

    # flask applicatie aanmaken
    app = Flask(application_name)

    # load configuration settings for this app
    cfg = load_configuration(base_dir, config_name, settings_override)
    app.config.update(cfg)

    # Initialize logging
    os.chdir(base_dir)
    logconfig = os.path.join(base_dir, 'logging.yml')
    logging = setup_logging(default_path=logconfig)
    logger = logging.getLogger(__name__)

    logger.info("Starting API server in {0} mode".format(config_name.upper()))

    # Use Flask-Restful for easy REST endpoints

    errors = {
        'EmptyStackException': {
            'message': "There are not enough items on the stack for this operation.",
            'status': 409,
        },
    }

    api = Api(catch_all_404s=True, errors=errors)
    add_resources(api)
    api.init_app(app)

    return app
