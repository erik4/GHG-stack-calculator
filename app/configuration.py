from flask.config import *
from config import config
import logging

logger = logging.getLogger(__name__)


def load_configuration(root_path, config_name, settings_override=None):
    """
    a generic function to load configuration variables from different places when the app starts.

    Args:
        root_path (str): the root path of the app
        config_name (str): the name of the configuration to load from config.py in the root
        settings_override (dict, optional): Optional dict of key/values to overrule specific settings

    Returns:
        :obj:`flask.config.Config` a Flask config object

    """
    cfg = Config(root_path)

    # load a generic config file in the root of the site for settings that can go into source control
    cfg.from_object(config[config_name])

    # load a specific config file in the 'instance' subfolder
    try:
        cfg.from_pyfile(os.path.join(root_path, 'instance', 'config.py'))
    except FileNotFoundError:
        logger.critical("The config.py file is not found in the 'instance' subfolder.")

    # any setting can be overridden in the call to this method
    if settings_override:
        cfg.update(settings_override)

    return cfg