
class CalculationLogic(object):
    """ Business logic for the stack calculator API """

    @staticmethod
    def peek(stack):
        """
        returns stack[top]

        :param stack: the stack to check
        :return: the topmost integer from the stack
        """
        r = stack.peek()

        return r

    @staticmethod
    def push(stack, number):
        """
        pushes a number onto the stack

        :param stack: the stack to push to
        :param number: the integer to push
        :return: stack[top]
        """
        stack.push(number)
        r = stack.peek()

        return r

    @staticmethod
    def pop(stack):
        """
        returns the top from the stack and removes it

        :param stack: the stack to pop from
        :return: the popped integer
        """
        r = stack.pop()

        return r

    @staticmethod
    def add(stack):
        """
        removes the top and top-1 from the stack and replaces it with stack[top-1]+stack[top]

        :param stack: the stack for the operation
        :return: stack[top]
        """
        n1 = stack.pop()
        n2 = stack.pop()
        r = stack.push(n2+n1)

        return r

    @staticmethod
    def subtract(stack):
        """
        removes the top and top-1 from the stack and replaces it with stack[top-1]-stack[top]

        :param stack: the stack for the operation
        :return: stack[top]
        """
        n1 = stack.pop()
        n2 = stack.pop()
        r = stack.push(n2-n1)

        return r

    @staticmethod
    def multiply(stack):
        """
        removes the top and top-1 from the stack and replaces it with stack[top-1]*stack[top]

        :param stack: the stack for the operation
        :return: stack[top]
        """
        n1 = stack.pop()
        n2 = stack.pop()
        r = stack.push(n2*n1)

        return r

    @staticmethod
    def divide(stack):
        """
        removes the top and top-1 from the stack and replaces it with stack[top-1]/stack[top]

        :param stack: the stack for the operation
        :return: stack[top]
        """
        n1 = stack.pop()
        n2 = stack.pop()
        r = stack.push(n2//n1)

        return r
