import os
import logging
import logging.config
import yaml


def setup_logging(default_path='logging.yml', default_level=logging.INFO, env_key='LOG_CFG'):
    """
        Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.load(f.read())
        logging.config.dictConfig(config)
    else:
        # if for some reason the log config isn't found, fall back to a simple default
        print("Logging config file {0} not found. Using default configuration.".format(path))
        default_format = '%(asctime)s %(levelname)s: %(message)s  [in %(pathname)s:%(lineno)d]'
        logging.basicConfig(level=default_level, format=default_format)

    return logging