# This is a generic config file. It contains non-sensitive generic settings
# sensitive settings should go in the environment variables (os.environ.get())
# Machine or environment-specific settings go in instance/config, which should NOT be versioned

import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    LOGPATH = os.path.join(basedir, 'logs')
    BASEPATH = basedir

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = False
    ENV = "development"


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    ENV = "testing"


class ProductionConfig(Config):
    DEBUG = False
    ENV = "production"


config = {
    'baseconfig': Config,
    'development': DevelopmentConfig,
    'testing': TestConfig,
    'production': ProductionConfig,
    'default': ProductionConfig
    }
