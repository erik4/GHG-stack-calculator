import os
import app

config_name = None
if os.getenv('FLASK_CONFIG'):
    config_name = os.getenv('FLASK_CONFIG')
if not config_name:
    config_name = 'production'

# Create the Flask App
app = app.create_apiserver(config_name)

if __name__ == "__main__":
    app.run(threaded=True)
