from tests import BaseTestCase, unittest
from app.utils.stackmanager import RedisStack


class TestAPIFunctionality(BaseTestCase):

    def setUp(self):
        pass

    def test_push_URI(self):
        """
           Test push '2' onto the stack
        """
        rs = RedisStack(0)
        rs._clear()

        rv = self.client.post('/calc/0/push/2')

        # check if the pushed value is returned
        self.assertEqual(rv.json, 2)
        # check if the value is stored in Redis
        self.assertEqual(rs._store.lrange(rs.stackid, -1, -1), ['2'])

    def test_peek_URI(self):
        """
           Tests if the peek function returns the last pushed value
        """
        rs = RedisStack(0)
        rs._clear()

        rv = self.client.post('/calc/0/push/3')
        top_of_stack = self.client.get('/calc/0/peek')

        self.assertEqual(top_of_stack.json, 3)

    def test_pop_URI(self):
        """
           Tests if the last value can be popped
        """
        rs = RedisStack(0)
        rs._clear()

        rv = self.client.post('/calc/0/push/4')
        top_of_stack = self.client.delete('/calc/0/pop')
        # check if the correct value is popped
        self.assertEqual(top_of_stack.json, 4)
        # check if the value removed from Redis
        self.assertEqual(rs._store.lrange(rs.stackid, -1, -1), [])

    def test_add_URI(self):
        """
           Tests the add functionality and negative values
        """
        rs = RedisStack(0)
        rs._clear()

        arg1 = self.client.post('/calc/0/push/-2')
        arg2 = self.client.post('/calc/0/push/-2')
        rv = self.client.patch('/calc/0/add')

        self.assertEqual(rv.json, -4)

    def test_subtract_URI(self):
        """
           Tests the subtract functionality
        """
        rs = RedisStack(0)
        rs._clear()

        arg1 = self.client.post('/calc/0/push/-3')
        arg2 = self.client.post('/calc/0/push/4')
        rv = self.client.patch('/calc/0/subtract')

        self.assertEqual(rv.json, -7)

    def test_multiply_URI(self):
        """
           Tests multiply functionality
        """
        rs = RedisStack(0)
        rs._clear()

        arg1 = self.client.post('/calc/0/push/-2')
        arg2 = self.client.post('/calc/0/push/-5')
        rv = self.client.patch('/calc/0/multiply')

        self.assertEqual(rv.json, 10)

    def test_divide_URI(self):
        """
           Tests the divide functionality
        """
        rs = RedisStack(0)
        rs._clear()

        arg1 = self.client.post('/calc/0/push/1024')
        arg2 = self.client.post('/calc/0/push/8')
        rv = self.client.patch('/calc/0/divide')

        self.assertEqual(rv.json, 128)

    def test_second_stack_functionality(self):
        """
           Tests if an additional stack is functional
        """
        rs = RedisStack(0)
        rs._clear()
        rs1 = RedisStack(1)
        rs1._clear()

        arg1 = self.client.post('/calc/0/push/8')
        arg2 = self.client.post('/calc/1/push/9')
        arg3 = self.client.post('/calc/0/push/4')
        arg4 = self.client.post('/calc/1/push/3')

        result_stack_0 = self.client.patch('/calc/0/divide')
        result_stack_1 = self.client.patch('/calc/1/multiply')

        self.assertEqual(result_stack_0.json, 2)
        self.assertEqual(result_stack_1.json, 27)


if __name__ == '__main__':
    unittest.main()
