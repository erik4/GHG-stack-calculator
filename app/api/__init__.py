from app.api.views.calc import Peek, Push, Pop, Add, Subtract, Multiply, Divide

def add_resources(api):
    api.add_resource(Peek, '/calc/<int:stackidx>/peek')
    api.add_resource(Push, '/calc/<int:stackidx>/push/<string:number>')
    api.add_resource(Pop, '/calc/<int:stackidx>/pop')
    api.add_resource(Add, '/calc/<int:stackidx>/add')
    api.add_resource(Subtract, '/calc/<int:stackidx>/subtract')
    api.add_resource(Multiply, '/calc/<int:stackidx>/multiply')
    api.add_resource(Divide, '/calc/<int:stackidx>/divide')
