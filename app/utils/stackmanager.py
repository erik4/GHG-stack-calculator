import redis
from flask import current_app
from app.utils.constants import Constant
from app.exceptions import EmptyStackException


class RedisStackManager(object):
    """
    Implementations need a collection of stacks to work on
    Stackmanager keeps track of the stacks
    Stackmanager for a Redis based back end
    """

    def __init__(self):
        self._stacks = {}
        self._currentstack=0

    @property
    def stacks(self):
        return self._stacks[self._currentstack]

    @stacks.setter
    def stacks(self, value):
        self._stacks[self._currentstack] = value

    def __getitem__(self, idx):
        """
        Create a new stack if the requested stack index is the next available index
        """
        if idx not in self._stacks:
            self._currentstack = idx
            self._stacks[idx] = RedisStack(idx)
        return self._stacks[idx]

    def __setitem__(self, idx, value):
        self._stacks[idx] = value


class ListStackManager(object):
    """
    Implementations need a collection of stacks to work on
    Stackmanager keeps track of the stacks
    Stackmanager for a Python list based back end
    """

    def __init__(self):
        self._stacks = []

    @property
    def stacks(self):
        return self._stacks

    @stacks.setter
    def stacks(self, value):
        self._stacks = value

    def __getitem__(self, idx):
        if len(self._stacks) >= idx:
            return self._stacks[idx]

    def __setitem__(self, idx, value):
        self._stacks[idx] = value


class BaseStack(object):
    """
    add, subtract, multiply and divide are the same operations using a few key methods (push, pop and peek).
    These calculation functions go in the businesslogic, the stack implementation provides push, pop and peek
    """

    def peek(self):
        raise NotImplementedError("Please Implement this method")

    def push(self, number):
        raise NotImplementedError("Please Implement this method")

    def pop(self):
        raise NotImplementedError("Please Implement this method")


class RedisStack(BaseStack):
    """
        Redis stack implementation
    """

    def __init__(self, idx=0):
        super(RedisStack, self).__init__()
        if current_app.config['REDIS_HOST'] and current_app.config['REDIS_DB']:
            self._store = redis.StrictRedis(host=current_app.config['REDIS_HOST'],
                                            db=current_app.config['REDIS_DB'],
                                            encoding='utf-8',
                                            decode_responses=True)
        else:
            raise ValueError("Please configure REDIS_HOST and REDIS_DB to use the Redis backend")

        self._idx = idx
        """
        Redis lists are identified by strings, not integers. To accomodate integer-based stack indexes we need to
        change the index into a string.
        This function creates a recognisable (string) name for stack indexes
        """
        self.stackid = Constant.Calculator.Stack.NAMEPREFIX + str(self._idx)

    def _clear(self):
        """
        a clear method to wipe out a stack. Used for the integration tests where we need a clean slate.
        :return: None
        """
        self._store.delete(self.stackid)

    def peek(self):
        """
        returns stack[top]

        :return: the topmost integer from the stack
        """
        r = self._store.lrange(self.stackid, -1, -1)
        if len(r):
            return int(r[0])
        else:
            raise EmptyStackException("This stack is empty")

    def push(self, number):
        """
        pushes a number onto the stack

        :param number: the integer to push
        :return: stack[top]
        """
        self._store.rpush(self.stackid, number)
        r = self.peek()
        return r

    def pop(self):
        """
        returns the top from the stack and removes it

        :return: the popped integer
        """
        stackvalue = self._store.rpop(self.stackid)
        if stackvalue is None:
            raise EmptyStackException("There are not enough items on the stack for this operation.")

        r = int(stackvalue)
        return r


class ListStack(BaseStack):
    """
        Simple stack implementation without a storage backend for testing purposes
    """

    def __init__(self, *args):
        super(ListStack, self).__init__()
        self._stack = []
        self._stack.extend(args)

    def peek(self):
        """
        returns stack[top]

        :return: the topmost integer from the stack
        """
        r = self._stack[-1]

        return r

    def push(self, number):
        """
        pushes a number onto the stack

        :param number: the integer to push
        :return: stack[top]
        """
        self._stack.append(number)
        r = self.peek()

        return r

    def pop(self):
        """
        returns the top from the stack and removes it

        :return: the popped integer
        """
        r = self._stack.pop()

        return r

